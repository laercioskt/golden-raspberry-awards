package br.dev.laercio.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static java.lang.Integer.MAX_VALUE;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

public class Producer {

    private final String producer;
    private final List<Movie> movies;

    public Producer(String producer, List<Movie> movies) {
        this.producer = producer;
        this.movies = movies;
    }

    public List<WinnerAwardsInterval> minAwardsInterval() {
        List<WinnerAwardsInterval> awardsIntervals = new ArrayList<>();

        List<Movie> movies = moviesOrderByYear();
        ListIterator<Movie> movieListIterator = movies.listIterator();
        int lastDifference = MAX_VALUE;
        while (movieListIterator.hasNext()) {
            int previousIndex = movieListIterator.previousIndex();
            Movie movie = movieListIterator.next();
            Movie previousMovie = previousIndex == -1 ? null : moviesOrderByYear().get(previousIndex);
            Movie nextMovie = movieListIterator.nextIndex() == moviesOrderByYear().size() ? null : moviesOrderByYear().get(movieListIterator.nextIndex());

            if (hasOnlyOne(previousMovie, nextMovie)) break;
            if (isTheFirst(previousMovie)) continue;

            int currentDifference = movie.getYear() - previousMovie.getYear();
            if (currentDifference < lastDifference){
                lastDifference = currentDifference;
                awardsIntervals.clear();
                awardsIntervals.add(new WinnerAwardsInterval(producer, currentDifference, previousMovie.getYear(), movie.getYear()));
            } else if (currentDifference == lastDifference){
                awardsIntervals.add(new WinnerAwardsInterval(producer, currentDifference, previousMovie.getYear(), movie.getYear()));
            }
        }

        return awardsIntervals;
    }

    public List<WinnerAwardsInterval> maxAwardsInterval() {
        List<WinnerAwardsInterval> awardsIntervals = new ArrayList<>();

        List<Movie> movies = moviesOrderByYear();
        ListIterator<Movie> movieListIterator = movies.listIterator();
        int lastDifference = 0;
        while (movieListIterator.hasNext()) {
            int previousIndex = movieListIterator.previousIndex();
            Movie movie = movieListIterator.next();
            Movie previousMovie = previousIndex == -1 ? null : moviesOrderByYear().get(previousIndex);
            Movie nextMovie = movieListIterator.nextIndex() == moviesOrderByYear().size() ? null : moviesOrderByYear().get(movieListIterator.nextIndex());

            if (hasOnlyOne(previousMovie, nextMovie)) break;
            if (isTheFirst(previousMovie)) continue;

            int currentDifference = movie.getYear() - previousMovie.getYear();
            if (currentDifference > lastDifference){
                lastDifference = currentDifference;
                awardsIntervals.clear();
                awardsIntervals.add(new WinnerAwardsInterval(producer, currentDifference, previousMovie.getYear(), movie.getYear()));
            } else if (currentDifference == lastDifference){
                awardsIntervals.add(new WinnerAwardsInterval(producer, currentDifference, previousMovie.getYear(), movie.getYear()));
            }
        }

        return awardsIntervals;
    }

    private boolean isTheFirst(Movie previousMovie) {
        return previousMovie == null;
    }

    private boolean hasOnlyOne(Movie previousMovie, Movie nextMovie) {
        return previousMovie == null && nextMovie == null;
    }

    private List<Movie> moviesOrderByYear() {
        return movies.stream().sorted(comparing(Movie::getYear)).collect(toList());
    }

}
