package br.dev.laercio.model;

import com.opencsv.bean.CsvBindByPosition;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.List;

import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;

@Entity
public class Movie implements Serializable {

    @Id
    @GeneratedValue(generator = "movie_generator")
    @SequenceGenerator(name = "movie_generator", sequenceName = "movie_sequence")
    private Long id;

    @NotBlank
    @CsvBindByPosition(position = 0)
    private String year;

    @NotBlank
    @CsvBindByPosition(position = 1)
    private String title;

    @NotBlank
    @CsvBindByPosition(position = 2)
    private String studios;

    @NotBlank
    @CsvBindByPosition(position = 3)
    private String producer;

    @CsvBindByPosition(position = 4)
    private String winner;

    public Movie() {
    }

    public Movie(String year, String title, String studios, String producer, String winner) {
        this.year = year;
        this.title = title;
        this.studios = studios;
        this.producer = producer;
        this.winner = winner;
    }

    public Integer getYear() {
        return Integer.valueOf(year);
    }

    public String getTitle() {
        return title;
    }

    public String getStudios() {
        return studios;
    }

    public String getProducer() {
        return producer;
    }

    public String getWinner() {
        return winner;
    }

    public boolean isWinner() {
        return "yes".equals(getWinner());
    }

    public List<String> getProducers() {
        return stream(producer.split("(, | and )"))
                .map(p -> p.replaceFirst("and ", ""))
                .collect(toList());
    }
}

