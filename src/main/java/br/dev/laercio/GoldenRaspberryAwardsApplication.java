package br.dev.laercio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class GoldenRaspberryAwardsApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoldenRaspberryAwardsApplication.class, args);
    }

}

