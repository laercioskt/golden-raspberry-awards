package br.dev.laercio.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class Movies extends ArrayList<Movie> {

    public Movies() {
    }

    public Movies(List<Movie> movies) {
        super(movies);
    }

    public Producers getWinnersGroupedByProducers() {
        Stream<Movie> moviesSplitByProducers = winners().flatMap(splitWhenMultipleProducers());
        Stream<Producer> mapToProducerObject = groupAndMapToProducer(moviesSplitByProducers);
        return mapToProducerObject.collect(collectingAndThen(toList(), Producers::new));
    }

    private Stream<Producer> groupAndMapToProducer(Stream<Movie> moviesSplitByProducers) {
        Stream<Entry<String, List<Movie>>> mapOfProducerAndMovies = moviesSplitByProducers.collect(groupingBy(Movie::getProducer)).entrySet().stream();
        return mapOfProducerAndMovies.map(producerMovies -> new Producer(producerMovies.getKey(), producerMovies.getValue()));
    }

    private Function<Movie, Stream<Movie>> splitWhenMultipleProducers() {
        return movie -> movie.getProducers().stream()
                .map(producer -> new Movie(movie.getYear().toString(), movie.getTitle(),
                        movie.getStudios(), producer, movie.getWinner()));
    }

    private Stream<Movie> winners() {
        return stream().filter(Movie::isWinner);
    }
}
