package br.dev.laercio.database;

import br.dev.laercio.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class InitialDataBaseLoader implements ApplicationRunner {

    public static final String INITIAL_DATA_BASE_FILE_NAME = "movielist.csv";

    @Autowired
    private MovieRepository movieRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        new MovieCSVReader().movies(INITIAL_DATA_BASE_FILE_NAME)
                .forEach(movie -> movieRepository.save(movie));
    }
}
