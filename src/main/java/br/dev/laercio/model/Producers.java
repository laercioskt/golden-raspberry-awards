package br.dev.laercio.model;

import java.util.*;
import java.util.stream.Stream;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.groupingBy;

public class Producers extends ArrayList<Producer> {

    public Producers(List<Producer> producers) {
        super(producers);
    }

    public List<WinnerAwardsInterval> getWithMaxInterval() {
        return getMapOfIntervalAndWinners(getMaxAwardsIntervals()).stream()
                .max(comparingByKey())
                .map(Map.Entry::getValue)
                .orElse(new ArrayList<>());
    }

    public List<WinnerAwardsInterval> getWithMinInterval() {
        return getMapOfIntervalAndWinners(getMinAwardsIntervals()).stream()
                .min(comparingByKey())
                .map(Map.Entry::getValue)
                .orElse(new ArrayList<>());
    }

    private Stream<WinnerAwardsInterval> getMinAwardsIntervals() {
        return stream().map(Producer::minAwardsInterval).flatMap(Collection::stream);
    }

    private Stream<WinnerAwardsInterval> getMaxAwardsIntervals() {
        return stream().map(Producer::maxAwardsInterval).flatMap(Collection::stream);
    }

    private Set<Map.Entry<Integer, List<WinnerAwardsInterval>>> getMapOfIntervalAndWinners(Stream<WinnerAwardsInterval> winnersInterval) {
        return winnersInterval.collect(groupingBy(wai -> wai.interval)).entrySet();
    }

}
