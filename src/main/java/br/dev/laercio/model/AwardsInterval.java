package br.dev.laercio.model;

import java.util.List;

public class AwardsInterval {

    public final List<WinnerAwardsInterval> min;
    public final List<WinnerAwardsInterval> max;

    public AwardsInterval(List<WinnerAwardsInterval> min, List<WinnerAwardsInterval> max) {
        this.min = min;
        this.max = max;
    }
}
