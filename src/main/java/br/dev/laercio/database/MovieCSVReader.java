package br.dev.laercio.database;

import br.dev.laercio.model.Movie;
import br.dev.laercio.model.Movies;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Reader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.logging.Logger;

import static java.lang.String.format;
import static java.util.logging.Logger.getLogger;

public class MovieCSVReader {

    private static final Logger LOGGER = getLogger(MovieCSVReader.class.getName());

    public Movies movies(String fileName) {
        URL resource = getClass().getClassLoader().getResource(fileName);
        if (resource != null) {
            return loadAndReturnMovies(resource);
        } else {
            LOGGER.info(format("Resource with name %s not found", fileName));
            return new Movies();
        }
    }

    private Movies loadAndReturnMovies(URL resource) {
        try {
            Reader reader = new FileReader(new File(resource.toURI()));
            List<Movie> movies = new CsvToBeanBuilder<Movie>(reader)
                    .withType(Movie.class)
                    .withSkipLines(1)
                    .withSeparator(';')
                    .build().parse();
            return new Movies(movies);
        } catch (FileNotFoundException | URISyntaxException e) {
            LOGGER.warning(format("Could not open file %s", resource.getFile()));
            return new Movies();
        }
    }

}
