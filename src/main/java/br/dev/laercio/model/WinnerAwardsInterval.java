package br.dev.laercio.model;

public class WinnerAwardsInterval {

    public final String producer;
    public final Integer interval;
    public final Integer previousWin;
    public final Integer followingWin;

    public WinnerAwardsInterval(String producer, Integer interval, Integer previousWin, Integer followingWin) {
        this.producer = producer;
        this.interval = interval;
        this.previousWin = previousWin;
        this.followingWin = followingWin;
    }

}
