package br.dev.laercio.controller;

import br.dev.laercio.model.*;
import br.dev.laercio.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RestController
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;

    @PostMapping("/movies")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        return new ResponseEntity<>(movieRepository.save(movie), CREATED);
    }

    @GetMapping("/movies/awards/interval")
    public ResponseEntity<AwardsInterval> awardsInterval() {
        Movies movies = movieRepository.findAll();
        Producers producers = movies.getWinnersGroupedByProducers();
        List<WinnerAwardsInterval> min = producers.getWithMinInterval();
        List<WinnerAwardsInterval> max = producers.getWithMaxInterval();
        return new ResponseEntity<>(new AwardsInterval(min, max), OK);
    }

}
