package br.dev.laercio.repository;

import br.dev.laercio.model.Movie;
import br.dev.laercio.model.Movies;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

    Movies findAll();

}
