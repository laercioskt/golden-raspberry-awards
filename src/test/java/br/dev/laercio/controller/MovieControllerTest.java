package br.dev.laercio.controller;

import br.dev.laercio.context.TestConfig;
import br.dev.laercio.database.MovieCSVReader;
import br.dev.laercio.model.Movie;
import br.dev.laercio.repository.MovieRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@WebAppConfiguration
public class MovieControllerTest {

    private static final String EXAMPLE_DATA_BASE_FILE_NAME = "movielist.csv";
    private static final String MOVIE_LIST_PRODUCERS_WITH_AND_SEPARATOR_FILE_NAME = "movielistproducerswithANDseparator.csv";
    private static final String MOVIE_LIST_PRODUCERS_WITH_COMMA_SEPARATOR_FILE_NAME = "movielistproducerswithCOMMAseparator.csv";
    private static final String MOVIE_LIST_PRODUCERS_WITH_COMMA_AND_AND_SEPARATOR_FILE_NAME = "movielistproducerswithCOMMAAndANDseparator.csv";

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private MovieRepository movieRepository;

    private MockMvc mockMvc;

    @BeforeEach
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();
    }

    @AfterEach
    public void teardown() {
        movieRepository.deleteAll();
    }

    @Test
    public void givenTheLastAwardsWhenAskForIntervalsThenReturnProducersWinnersWithMaxAndMinInterval() throws Exception {
        importAllGoldenRaspberryAwardsMovies(EXAMPLE_DATA_BASE_FILE_NAME);

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Matthew Vaughn\"," +
                "            \"interval\": 13," +
                "            \"previousWin\": 2002," +
                "            \"followingWin\": 2015" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenAwardsWithAndSeparatorWhenAskForIntervalsThenReturnProducersWinnersWithMaxAndMinInterval() throws Exception {
        importAllGoldenRaspberryAwardsMovies(MOVIE_LIST_PRODUCERS_WITH_AND_SEPARATOR_FILE_NAME);

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenAwardsWithCommaSeparatorWhenAskForIntervalsThenReturnProducersWinnersWithMaxAndMinInterval() throws Exception {
        importAllGoldenRaspberryAwardsMovies(MOVIE_LIST_PRODUCERS_WITH_COMMA_SEPARATOR_FILE_NAME);

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenAwardsWithCommaAndAndSeparatorWhenAskForIntervalsThenReturnProducersWinnersWithMaxAndMinInterval() throws Exception {
        importAllGoldenRaspberryAwardsMovies(MOVIE_LIST_PRODUCERS_WITH_COMMA_AND_AND_SEPARATOR_FILE_NAME);

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenEmptyListOfMovieWhenAskForIntervalsThenReturnEmptyProducersInMaxAndMinInterval() throws Exception {
        String expected = "{" +
                "    \"min\": []," +
                "    \"max\": []" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenMoviesWithNoWinnerWhenAskForIntervalsThenReturnEmptyProducersInMaxAndMinInterval() throws Exception {
        movieRepository.save(new Movie("2000", "Title 1", "Studio 1", "Producer 1", null));
        movieRepository.save(new Movie("2001", "Title 2", "Studio 1", "Producer 1", null));
        movieRepository.save(new Movie("2005", "Title 3", "Studio 1", "Producer 2", null));
        movieRepository.save(new Movie("2006", "Title 4", "Studio 1", "Producer 2", null));

        String expected = "{" +
                "    \"min\": []," +
                "    \"max\": []" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenFourWinnersTwoProducersSameIntervalWhenAskForIntervalsThenReturnSameProducersWithMaxAndMinInterval() throws Exception {
        movieRepository.save(new Movie("2000", "Title 1", "Studio 1", "Producer 1", "yes"));
        movieRepository.save(new Movie("2001", "Title 2", "Studio 1", "Producer 1", "yes"));
        movieRepository.save(new Movie("2005", "Title 3", "Studio 1", "Producer 2", "yes"));
        movieRepository.save(new Movie("2006", "Title 4", "Studio 1", "Producer 2", "yes"));

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Producer 1\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2000," +
                "            \"followingWin\": 2001" +
                "        }," +
                "        {" +
                "            \"producer\": \"Producer 2\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2005," +
                "            \"followingWin\": 2006" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Producer 1\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2000," +
                "            \"followingWin\": 2001" +
                "        }," +
                "        {" +
                "            \"producer\": \"Producer 2\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2005," +
                "            \"followingWin\": 2006" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenPostsToCreateNewMovieWhenAskForIntervalsThenReturnProducer() throws Exception {
        this.mockMvc
                .perform(post("/movies")
                        .content(asJsonString(new Movie("2000", "Title 1", "Studios 1", "Producer", "yes")))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());

        this.mockMvc
                .perform(post("/movies")
                        .content(asJsonString(new Movie("2001", "Title 2", "Studios 1", "Producer", "yes")))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Producer\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2000," +
                "            \"followingWin\": 2001" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Producer\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2000," +
                "            \"followingWin\": 2001" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenTheLastAwardsWhenAddTwoTitlesWithMaxDifferenceAndAskForIntervalsThenReturnSameProducerTwoTimesInMaxInterval() throws Exception {
        importAllGoldenRaspberryAwardsMovies(EXAMPLE_DATA_BASE_FILE_NAME);

        this.mockMvc
                .perform(post("/movies")
                        .content(asJsonString(new Movie("1980", "Test1", "Test1", "Matthew Vaughn", "yes")))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());

        this.mockMvc
                .perform(post("/movies")
                        .content(asJsonString(new Movie("2037", "Test2", "Test2", "Matthew Vaughn", "yes")))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Matthew Vaughn\"," +
                "            \"interval\": 22," +
                "            \"previousWin\": 1980," +
                "            \"followingWin\": 2002" +
                "        }," +
                "        {" +
                "            \"producer\": \"Matthew Vaughn\"," +
                "            \"interval\": 22," +
                "            \"previousWin\": 2015," +
                "            \"followingWin\": 2037" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenTheLastAwardsWhenAddNewMoviesThatRepresentsFourConsecutiveWinsAndAskForIntervalsThenReturnSameProducerThreeTimes() throws Exception {
        importAllGoldenRaspberryAwardsMovies(EXAMPLE_DATA_BASE_FILE_NAME);

        this.mockMvc
                .perform(post("/movies")
                        .content(asJsonString(new Movie("1989", "Test1", "Test1", "Joel Silver", "yes")))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());

        this.mockMvc
                .perform(post("/movies")
                        .content(asJsonString(new Movie("1992", "Test2", "Test2", "Joel Silver", "yes")))
                        .contentType(APPLICATION_JSON)
                        .accept(APPLICATION_JSON))
                .andExpect(status().isCreated());

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1989," +
                "            \"followingWin\": 1990" +
                "        }," +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1990," +
                "            \"followingWin\": 1991" +
                "        }," +
                "        {" +
                "            \"producer\": \"Joel Silver\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 1991," +
                "            \"followingWin\": 1992" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Matthew Vaughn\"," +
                "            \"interval\": 13," +
                "            \"previousWin\": 2002," +
                "            \"followingWin\": 2015" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    @Test
    public void givenProducerWithMinAndMaxWhenAskForIntervalsThenReturnProducerInBothList() throws Exception {
        movieRepository.save(new Movie("2000", "Title 1", "Studio 1", "Producer 1", "yes"));
        movieRepository.save(new Movie("2001", "Title 2", "Studio 1", "Producer 1", "yes"));

        movieRepository.save(new Movie("2006", "Title 3", "Studio 1", "Producer 2", "yes"));
        movieRepository.save(new Movie("2007", "Title 4", "Studio 1", "Producer 2", "yes"));

        movieRepository.save(new Movie("2005", "Title 5", "Studio 1", "Producer 1", "yes"));
        movieRepository.save(new Movie("2010", "Title 6", "Studio 1", "Producer 1", "yes"));

        String expected = "{" +
                "    \"min\": [" +
                "        {" +
                "            \"producer\": \"Producer 1\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2000," +
                "            \"followingWin\": 2001" +
                "        }," +
                "        {" +
                "            \"producer\": \"Producer 2\"," +
                "            \"interval\": 1," +
                "            \"previousWin\": 2006," +
                "            \"followingWin\": 2007" +
                "        }" +
                "    ]," +
                "    \"max\": [" +
                "        {" +
                "            \"producer\": \"Producer 1\"," +
                "            \"interval\": 5," +
                "            \"previousWin\": 2005," +
                "            \"followingWin\": 2010" +
                "        }" +
                "    ]" +
                "}";

        this.mockMvc
                .perform(get("/movies/awards/interval"))
                .andDo(print())
                .andExpect(content().json(expected));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void importAllGoldenRaspberryAwardsMovies(String fileName) {
        new MovieCSVReader().movies(fileName).forEach(movie -> movieRepository.save(movie));
    }

}