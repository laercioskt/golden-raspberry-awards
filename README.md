***

## Name
Golden Raspberry Awards.

## Description
This project offers APIs to get information about Golden Raspberry Awards.

## Installation
<a href="https://docs.oracle.com/cd/E19182-01/821-0917/inst_jdk_javahome_t/index.html" target="_blank">Configure JAVA_HOME environment</a> variable if is not already configure.

## Usage
Compile the project on Windows:
```
.\mvnw.cmd compile
```
or on Linux:
```
sh mvnw compile
```
Starting service with:
```
.\mvnw.cmd spring-boot:run
```
or
```
sh mvnw spring-boot:run
```

Address of the service:
```
http://localhost:8080
```

Example of a method that return producers who won awards in the minimum and the maximum interval of time:
```
Request: GET http://localhost:8080/movies/awards/interval
Response:
{
    "min": [
        {
            "producer": "Joel Silver",
            "interval": 1,
            "previousWin": 1990,
            "followingWin": 1991
        }
    ],
    "max": [
        {
            "producer": "Matthew Vaughn",
            "interval": 13,
            "previousWin": 2002,
            "followingWin": 2015
        }
    ]
}
```
Example adding movie:
```
Request: POST http://localhost:8080/movies
Body:
{
    "year": "2001",
    "title": "title 2",
    "studios": "studios 2",
    "producer": "producer 2",
    "winner": "yes"
}
Response:
{
    "year": 2001,
    "title": "title 2",
    "studios": "studios 2",
    "producer": "producer 2",
    "winner": "yes",
    "producers": [
        "producer 2"
    ]
}
```

## Running tests
Run tests with:
```
.\mvnw.cmd test
```
or
```
sh mvnw test
```